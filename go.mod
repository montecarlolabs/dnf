module gitlab.com/montecarlolabs/dnf

go 1.19

require golang.org/x/exp v0.0.0-20220722155223-a9213eeb770e

require github.com/google/go-cmp v0.5.8
