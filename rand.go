package dnf

type Rand interface {
	Uint64() uint64
}

func Randomize(r Rand, x []bool) {
	for i := 0; i < len(x); {
		v := r.Uint64()
		for j := uint64(1); j > 0 && i < len(x); i, j = i+1, j<<1 {
			switch v & j {
			case 0:
				x[i] = false
			default:
				x[i] = true
			}
		}
	}
}
