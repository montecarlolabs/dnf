package dnf

import "math"

func Count(r Rand, e Expr, xi []bool) float64 {
	const n = 10_000
	x := make([]bool, len(xi))
	copy(x, xi)
	var res int
	for i := 0; i < n; i++ {
		if Randomize(r, x); e.Test(x) {
			res++
		}
	}
	return float64(res) / n * math.Pow(2, float64(len(x)-1))
}
