package dnf

import (
	"io"
	"sort"
	"strconv"
	"strings"

	"golang.org/x/exp/slices"
)

// Expr represents a SAT expression with 0 or more Terms.
type Expr []Term

// Empty returns whether the expression is empty.
//
// Empty expressions are not satisfiable.
func (e Expr) Empty() bool {
	return len(e) == 0
}

// Canonicalize reduces the expression to its canonical form.
//
// This includes sorting Terms and compacting redundant ones.
func (e *Expr) Canonicalize() {
	for i := range *e {
		(*e)[i].Normalize()
	}
	sort.Slice(*e, func(i, j int) bool {
		a, b := (*e)[i], (*e)[j]
		if len(a) != len(b) {
			return len(a) < len(b)
		}
		for i := range *e {
			if a[i] != b[i] {
				return false
			}
		}
		return true
	})
	for _, t := range *e {
		if t.Empty() {
			*e = Expr{}
			return
		}
	}
	slices.CompactFunc(*e, func(a, b Term) bool {
		if len(a) != len(b) {
			return false
		}
		for i := range a {
			if a[i] != b[i] {
				return false
			}
		}
		return true
	})
}

// Test tests whether x satisfies the expression e.
func (e Expr) Test(x []bool) bool {
	for i := range e {
		if e[i].Test(x) {
			return true
		}
	}
	return len(e) == 0 && len(x) == 0
}

// AppendFormat appends the formatted expression e to the writer w.
//
// Consider canonicalizing the expression with Canonicalize before calling.
func (e Expr) AppendFormat(w io.Writer) error {
	const join = "∨"
	for i := range e {
		if err := e[i].AppendFormat(w); err != nil {
			return err
		}
		if _, err := w.Write([]byte(join)); err != nil {
			return err
		}
	}
	return nil
}

// String returns the string representation of the expression e.
//
// Consider canonicalizing the expression with Canonicalize before calling.
func (e Expr) String() string {
	var sb strings.Builder
	e.AppendFormat(&sb)
	return sb.String()
}

// Term represents a SAT term.
type Term []Var

// Empty returns whether the term is empty.
//
// Empty terms are not satisfiable.
func (t Term) Empty() bool {
	return len(t) == 0
}

// Normalize sorts and deduplicates DNF Terms.
//
// The result is sorted and compacted to remove duplicates.
// If the term is unsatisfiable a new unsatisfiable empty term is returned.
func (t *Term) Normalize() {
	sort.Slice(*t, func(i, j int) bool {
		a, b := (*t)[i], (*t)[j]
		if ai, bi := a.Index(), b.Index(); ai != bi {
			return ai < bi
		}
		if an, bn := a.Negated(), b.Negated(); an != bn {
			return bn
		}
		return true
	})
	// Remove duplicates.
	*t = slices.Compact(*t)
	// Detect conflicts and reduce.
	var prevIndex int
	for i := range *t {
		idx := (*t)[i].Index()
		if i > 0 && idx == prevIndex {
			*t = Term{}
			break
		}
		prevIndex = idx
	}
}

// Test tests whether x satisfies the Term t.
func (t Term) Test(x []bool) bool {
	if t.Empty() {
		return false
	}
	for _, v := range t {
		if !v.Test(x) {
			return false
		}
	}
	return true
}

// AppendFormat appends the term t to the writer w.
func (t Term) AppendFormat(w io.Writer) error {
	const join = "∧"
	if _, err := w.Write([]byte{'('}); err != nil {
		return err
	}
	for i, v := range t {
		if err := v.AppendFormat(w); err != nil {
			return err
		}
		if len(t) <= i+1 {
			break
		}
		if _, err := w.Write([]byte(join)); err != nil {
			return err
		}
	}
	_, err := w.Write([]byte{')'})
	return err
}

// String returns the string representation of this term.
func (t Term) String() string {
	var sb strings.Builder
	t.AppendFormat(&sb)
	return sb.String()
}

// Var represents a SAT variable.
type Var int

// V creates a new Var.
func V(x int) Var {
	return Var(x)
}

// Negated returns whether the variable is negated.
func (v Var) Negated() bool { return v < 0 }

// Index returns the unsigned index of the variable.
func (v Var) Index() int {
	if v.Negated() {
		return int(-v)
	}
	return int(v)
}

// Test returns whether x satisfies the variable v.
func (v Var) Test(x []bool) bool {
	return x[v.Index()] != v.Negated()
}

// AppendFormat appends the formatted variable v to the writer w.
func (v Var) AppendFormat(w io.Writer) error {
	var b []byte
	const negate = "¬"
	if v.Negated() {
		if _, err := w.Write([]byte(negate)); err != nil {
			return err
		}
	}
	b = strconv.AppendUint(b, uint64(v.Index()), 10)
	_, err := w.Write(b)
	return err
}

// String returns the string representation of the variable v.
func (v Var) String() string {
	var sb strings.Builder
	v.AppendFormat(&sb)
	return sb.String()
}
