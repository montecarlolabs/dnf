package dnf

import (
	"math/rand"
	"testing"
)

func TestCount(t *testing.T) {
	// 0 0 1
	// 1 0 1
	// 1 1 0
	e := Expr{{V(1), V(2), V(-3)}, {V(-2), V(3)}}
	x := []bool{false, false, false, true}
	r := rand.New(rand.NewSource(1337))
	t.Log(Count(r, e, x))
}
