package dnf

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestTermSAT(t *testing.T) {
	for _, tc := range []struct {
		name    string
		input   Term
		x       []bool
		wantSAT bool
	}{{
		name:    "empty",
		wantSAT: false,
	}, {
		name:    "x=1 satisfies x",
		input:   Term{V(1)},
		x:       []bool{false, true},
		wantSAT: true,
	}, {
		name:    "x=0 satisfies ¬x",
		input:   Term{V(-1)},
		x:       []bool{false, false},
		wantSAT: true,
	}, {
		name:  "x=0 fails to satisfy x",
		input: Term{V(1)},
		x:     []bool{false, false},
	}, {
		name:  "x=1 fails to satisfy ¬x",
		input: Term{V(-1)},
		x:     []bool{false, true},
	}, {
		name: "(x,y,z)=(0,0,1) satisfies (¬x∧¬y∧z)",
		input: Term{
			V(-1),
			V(-2),
			V(3),
		},
		x:       []bool{false, false, false, true},
		wantSAT: true,
	}, {
		name: "(x,y,z)=(0,1,1) fails to satisfy (¬x∧¬y∧z)",
		input: Term{
			V(-1),
			V(-2),
			V(3),
		},
		x: []bool{false, false, true, true},
	}} {
		t.Run(tc.name, func(t *testing.T) {
			if got, want := tc.input.Test(tc.x), tc.wantSAT; got != want {
				t.Errorf("SAT(%s): %s: got %v, want %v", tc.name, tc.input.String(), got, want)
			}
		})
	}
}

func TestTermNormalize(t *testing.T) {
	for _, tc := range []struct {
		name  string
		input Term
		want  Term
	}{{
		name: "empty",
	}, {
		name:  "conflict",
		input: Term{V(1), V(2), V(-2)},
		want:  Term{},
	}, {
		name:  "sort",
		input: Term{V(3), V(2), V(1)},
		want:  Term{V(1), V(2), V(3)},
	}} {
		t.Run(tc.name, func(t *testing.T) {
			got := tc.input
			got.Normalize()
			if diff := cmp.Diff(tc.want, got); diff != "" {
				t.Errorf("Normalize(%q): got diff:\n%s", tc.name, diff)
			}
		})
	}
}
